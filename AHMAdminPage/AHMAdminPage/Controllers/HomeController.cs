﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AHMAdminPage.Models;
using AHMAdminPage.Services;
using System.IO;
using PagedList;

namespace AHMAdminPage.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string sortOrder, string currentFilter, string FullName, string StartDate, string EndDate, int? page, string sortOrder2, string currentFilter2, string FullName2, string StartDate2, string EndDate2, int? page2)
        {
            ViewBag.CreatedDateSortParam = String.IsNullOrEmpty(sortOrder) || sortOrder.Equals("Id Asc") ? "Id desc" : "Id Asc";
            ViewBag.CreatedDateSortParam2 = String.IsNullOrEmpty(sortOrder2) || sortOrder2.Equals("Id Asc") ? "Id desc" : "Id Asc";

            ViewBag.CurrentSort = sortOrder;

            if (FullName != null)
                page = 1;
            else
                FullName = currentFilter;

            if (FullName2 != null)
                page2 = 1;
            else
                FullName2 = currentFilter2;

            ViewBag.CurrentFilter = FullName;
            ViewBag.CurrentFilter2 = FullName2;

            DateTime? sDate = null;
            DateTime? eDate = null;
            DateTime? sDate2 = null;
            DateTime? eDate2 = null;

            if (string.IsNullOrEmpty(StartDate))
                sDate = null;
            else
            {
                DateTime temp;
                if (DateTime.TryParse(StartDate, out temp))
                    sDate = (DateTime)temp;
            }
            if (string.IsNullOrEmpty(EndDate))
                eDate = null;
            else
            {
                DateTime temp;
                if (DateTime.TryParse(EndDate, out temp))
                    eDate = (DateTime)temp;
            }

            if (string.IsNullOrEmpty(StartDate2))
                sDate2 = null;
            else
            {
                DateTime temp;
                if (DateTime.TryParse(StartDate2, out temp))
                    sDate2 = (DateTime)temp;
            }
            if (string.IsNullOrEmpty(EndDate2))
                eDate2 = null;
            else
            {
                DateTime temp;
                if (DateTime.TryParse(EndDate2, out temp))
                    eDate2 = (DateTime)temp;
            }

            List<UserUploadFiles> userUploadData = new List<UserUploadFiles>();
            List<UserResumeDetails> userResumeData = new List<UserResumeDetails>();

            GetUserUploadFromDB service = new GetUserUploadFromDB();
            userUploadData = service.GetUserUploadFiles(FullName, sDate, eDate, sortOrder);
            userResumeData = service.GetUserResumeDetails(FullName2, sDate2, sDate2, sortOrder2);


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            int pageNumber2 = (page2 ?? 1);

            UserDataModelView mv = new UserDataModelView();
            mv.UserUploadFilesList = userUploadData.ToPagedList(pageNumber, pageSize);
            mv.userResumeDetailsList = userResumeData.ToPagedList(pageNumber2, pageSize);

            return View(mv);



            //eturn View(data);
        }

        //[HttpPost]
        //public ActionResult Index(string DownloadId, string DownloadPath)
        //{
        //    return File(DownloadPath, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(DownloadPath));
        //    //return RedirectToAction("Index");
        //}

        
        public ActionResult DownloadFile(string DownloadId, string DownloadPath)
        {
            if (DownloadPath.Trim().Equals("-"))
            {
                return View("Index");
            }else
            {
                return File(DownloadPath, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(DownloadPath));
            }
            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

    }
}