﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;

namespace AHMAdminPage.Models
{
    public class UserDataModelView
    {
        public IPagedList<UserResumeDetails> userResumeDetailsList { get; set; }
        public IPagedList<UserUploadFiles> UserUploadFilesList { get; set; }
    }
}