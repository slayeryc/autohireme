﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AHMAdminPage.Models
{
    public class UserResumeDetails
    {

        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DOB { get; set; }
        public string PlaceOfResidence { get; set; }
        public string EducationLevel { get; set; }
        public string EducationLevelOther { get; set; }
        public string ExperienceYears { get; set; }
        public string CurrentPosition { get; set; }
        public string CurrentPositionOther { get; set; }
        //public Dictionary<string, string> PreferredPositions { get; set; }
        public string PreferredPosition { get; set; }
        public string PreferredPositionOther { get; set; }
        public int? ExpectedSalary { get; set; }
        public DateTime? createdDate { get; set; }
    }
}