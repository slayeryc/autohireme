﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AHMAdminPage.Models
{
    public class UserUploadFiles
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string PreferredPosition { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}