﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using AHMAdminPage.Models;

namespace AHMAdminPage.Services
{
    public class GetUserUploadFromDB
    {
        private readonly string connectionString = "server=127.0.0.1;uid=root;pwd=Autohireme0801;database=auto1";


        public List<UserUploadFiles> GetUserUploadFiles(string FullName, DateTime? StartDate, DateTime? EndDate, string SortName)
        {
            MySqlConnection connection = null;
            List<UserUploadFiles> data = new List<UserUploadFiles>();

            if (SortName != null && SortName.Equals("Id Asc"))
                SortName = "Id Asc";
            else
                SortName = "Id Desc";

            try
            {
                if (string.IsNullOrEmpty(FullName))
                    FullName = null;

                if (StartDate != null)
                    StartDate = ((DateTime)StartDate).ToUniversalTime();

                if (EndDate != null)
                {
                    DateTime tempEndDate = (DateTime)EndDate;
                    tempEndDate = tempEndDate.AddDays(1).AddSeconds(-1);
                    EndDate = tempEndDate.ToUniversalTime();
                }
                    

                connection = new MySqlConnection(connectionString);
                connection.Open();
                MySqlCommand sqlcmd = new MySqlCommand("SELECT * " + 
                        " FROM UserUploadFiles " + 
                        " WHERE(@FullName IS NULL OR FullName like concat('%', REPLACE(@FullName, ' ', '%'), '%')) " +
                        " AND(@StartDateUTC IS NULL OR CreatedDate_UTC >= @StartDateUTC) " +
                        " AND(@EndDateUTC IS NULL OR CreatedDate_UTC <= @EndDateUTC) " + 
                        " ORDER BY " + SortName + "; ", connection);
                sqlcmd.CommandType = System.Data.CommandType.Text;
                sqlcmd.CommandTimeout = 120;
                sqlcmd.Parameters.AddWithValue("@FullName", FullName);
                sqlcmd.Parameters.AddWithValue("@StartDateUTC", StartDate);
                sqlcmd.Parameters.AddWithValue("@EndDateUTC", EndDate);

                MySqlDataReader reader = sqlcmd.ExecuteReader();

                while (reader.Read())
                {
                    UserUploadFiles u = new UserUploadFiles();
                    //string xx = reader["ss"].ToString();
                    u.Id = int.Parse(reader["Id"].ToString());

                    object oFilePath = reader["FilePath"];
                    if (oFilePath != DBNull.Value)
                        u.FilePath = oFilePath.ToString();
                    else
                        u.FilePath = "-";
                    u.FullName = reader["FullName"].ToString();
                    DateTime utcDate = (DateTime)reader["CreatedDate_UTC"];
                    u.CreatedDate = utcDate.ToLocalTime();

                    object oEmail = reader["Email"];
                    if (oEmail != DBNull.Value)
                    {
                        u.EmailAddress = oEmail.ToString();
                    }
                    else
                    {
                        u.EmailAddress = "";
                    }
                    object oPhoneNumber = reader["PhoneNumber"];
                    if (oPhoneNumber != DBNull.Value)
                    {
                        u.PhoneNumber = oPhoneNumber.ToString();
                    }
                    else
                    {
                        u.PhoneNumber = "";
                    }

                    object oPreferredPosition = reader["PreferredPosition"];
                    if (oPreferredPosition != DBNull.Value)
                        u.PreferredPosition = DefaultData.getPreferredPosition(oPreferredPosition.ToString());
                    else
                        u.PreferredPosition = "";

                    data.Add(u);
                }

                reader.Close();

                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }


        public List<UserResumeDetails> GetUserResumeDetails(string FullName, DateTime? StartDate, DateTime? EndDate, string SortName)
        {
            MySqlConnection connection = null;
            List<UserResumeDetails> data = new List<UserResumeDetails>();

            if (SortName != null && SortName.Equals("Id Asc"))
                SortName = "Id Asc";
            else
                SortName = "Id Desc";

            try
            {
                if (string.IsNullOrEmpty(FullName))
                    FullName = null;

                if (StartDate != null)
                    StartDate = ((DateTime)StartDate).ToUniversalTime();

                if (EndDate != null)
                {
                    DateTime tempEndDate = (DateTime)EndDate;
                    tempEndDate = tempEndDate.AddDays(1).AddSeconds(-1);
                    EndDate = tempEndDate.ToUniversalTime();
                }


                connection = new MySqlConnection(connectionString);
                connection.Open();
                MySqlCommand sqlcmd = new MySqlCommand("SELECT * " +
                        " FROM UserResumeDetails " +
                        " WHERE(@FullName IS NULL OR FullName like concat('%', REPLACE(@FullName, ' ', '%'), '%')) " +
                        " AND(@StartDateUTC IS NULL OR CreatedDate_UTC >= @StartDateUTC) " +
                        " AND(@EndDateUTC IS NULL OR CreatedDate_UTC <= @EndDateUTC) " +
                        " ORDER BY " + SortName + "; ", connection);
                sqlcmd.CommandType = System.Data.CommandType.Text;
                sqlcmd.CommandTimeout = 120;
                sqlcmd.Parameters.AddWithValue("@FullName", FullName);
                sqlcmd.Parameters.AddWithValue("@StartDateUTC", StartDate);
                sqlcmd.Parameters.AddWithValue("@EndDateUTC", EndDate);

                MySqlDataReader reader = sqlcmd.ExecuteReader();

                while (reader.Read())
                {
                    UserResumeDetails u = new UserResumeDetails();
                    //string xx = reader["ss"].ToString();
                    u.Id = int.Parse(reader["Id"].ToString());
                    u.FullName = reader["FullName"].ToString();
                    DateTime utcDate = (DateTime)reader["CreatedDate_UTC"];
                    u.createdDate = utcDate.ToLocalTime();

                    object oEmail = reader["Email"];
                    if (oEmail != DBNull.Value)
                        u.Email = oEmail.ToString();
                    else
                        u.Email = "";
                  
                    object oPhoneNumber = reader["PhoneNumber"];
                    if (oPhoneNumber != DBNull.Value)
                        u.PhoneNumber = oPhoneNumber.ToString();
                    else
                        u.PhoneNumber = "";

                    object oDOB = reader["DOB"];
                    if (oDOB != DBNull.Value)
                        u.DOB = DateTime.Parse(oDOB.ToString());
                    else
                        u.DOB = null;

                    object oPlaceOfResidence = reader["PlaceOfResidence"];
                    if (oPlaceOfResidence != DBNull.Value)
                        u.PlaceOfResidence = DefaultData.getPlaceOfResidence(oPlaceOfResidence.ToString());
                    else
                        u.PlaceOfResidence = "";

                    object oEducationLevel = reader["EducationLevel"];
                    if (oEducationLevel != DBNull.Value)
                        u.EducationLevel = oEducationLevel.ToString();
                    else
                        u.EducationLevel = "";

                    object oExperienceYears = reader["ExperienceYears"];
                    int experience = 0;
                    if (oExperienceYears != DBNull.Value)
                    {
                        experience = int.Parse(oExperienceYears.ToString());
                        u.ExperienceYears = DefaultData.getExperienceYears(experience);
                    }
                    else
                        u.ExperienceYears = "";

                    object oCurrentPosition = reader["CurrentPosition"];
                    if (oCurrentPosition != DBNull.Value)
                        u.CurrentPosition = DefaultData.getCurrentPosition(oCurrentPosition.ToString());
                    else
                        u.CurrentPosition = "";

                    object oPreferredPosition = reader["PreferredPosition"];
                    if (oPreferredPosition != DBNull.Value)
                        u.PreferredPosition = DefaultData.getPreferredPosition(oPreferredPosition.ToString());
                    else
                        u.PreferredPosition = "";

                    object oExpectedSalary = reader["ExpectedSalary"];
                    if (oExpectedSalary != DBNull.Value)
                        u.ExpectedSalary = int.Parse(oExpectedSalary.ToString());
                    else
                        u.ExpectedSalary = 0;

                    data.Add(u);
                }

                reader.Close();

                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

    }
}