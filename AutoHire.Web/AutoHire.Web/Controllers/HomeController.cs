﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoHire.Web.Models;
using AutoHire.Web.Services;

namespace AutoHire.Web.Controllers
{
    public class HomeController : Controller
    {
        private static LoggingService log = new LoggingService();

        public ActionResult Index()
        {
            ViewBag.scripCall = "";
            return View();
        }

        [HttpPost]
        public ActionResult UploadFile(UploadMainModel model, HttpPostedFileBase file, string SubmitCV)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    bool serverValidation = true;
                    if (model == null)
                    {
                        ViewBag.scripCall = "DisplayMsgAfterUpload('Upload Error : Server error. Please contact support if this problem persist.');";
                        serverValidation = false;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(model.FullName))
                        {
                            ViewBag.scripCall = "DisplayMsgAfterUpload('Please fill in your full name.');";
                            serverValidation = false;
                        }
                        if (string.IsNullOrEmpty(model.Email))
                        {
                            ViewBag.scripCall = "DisplayMsgAfterUpload('Please fill in a valid email address.');";
                            serverValidation = false;
                        }
                    }

                    if (file.ContentLength <= 0 || file.ContentLength > 5120000)
                    {
                        ViewBag.scripCall = "DisplayMsgAfterUpload('upload failed : Please select a file with less than 5 MB of size. ');";
                        serverValidation = false;
                    }

                    if (serverValidation == true)
                    {
                        UserInputService service = new UserInputService();
                        int InsertedValue = service.InsertCVDetailsIntoDB(model);
                        if (InsertedValue > 0)
                        {
                            string FileName = InsertedValue.ToString() + "_" + Path.GetFileName(file.FileName);
                            //string path = Path.Combine(Server.MapPath("~/UploadedFiles"), FileName);
                            if (!Directory.Exists(GeneralSettingsService.UploadCVPath))
                                Directory.CreateDirectory(GeneralSettingsService.UploadCVPath);

                            string path = Path.Combine(GeneralSettingsService.UploadCVPath, FileName);
                            
                            file.SaveAs(path);

                            service.UpdateFilePathById(InsertedValue, path);
                            
                            ZohoMailService z = new ZohoMailService();
                            bool successEmail = z.SendConfirmationUploadEmail(InsertedValue, model.Email, model.FullName);

                            if (successEmail)
                            {
                                ViewBag.scripCall = "DisplayMsgAfterUpload('Thank you! Your application has been submitted successfully. We will contact you when we found a job matching for you.');";
                            }
                            
                            //ViewBag.Message = "'upload success'";
                            //model.FilePath = path;

                          

                        }
                        else
                        { //error insert into db 
                            ViewBag.scripCall = "DisplayMsgAfterUpload('Upload Error : Error DB01 - Please contact administrator if the problem persist.');";
                        }
                    }



                }
                catch (Exception ex)
                {
                    ViewBag.scripCall = "DisplayMsgAfterUpload('upload failed : " + ex.Message + "');";
                    log.WriteException(ex, "UploadFile");
                    //ViewBag.Message = ex.Message;
                    //throw;
                }
                //ModelState.Clear();
                ViewBag.scripCall = ViewBag.scripCall + "  window.location.href=\"/\"; ";
                return View("Index");
                //return RedirectToAction("Index");
            }
            else
            {
                string InvalidModel = "";
                foreach (ModelState ms in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in ms.Errors)
                    {
                        InvalidModel += error.ErrorMessage + "<br>";
                    }
                }
                ViewBag.scripCall = "DisplayMsgAfterUpload('upload failed : " + InvalidModel + "');";
                ViewBag.scripCall = ViewBag.scripCall + "  window.location.href=\"/\"; ";
                return View("Index", model);
                //return JavaScript("window.alert('upload success');");
            }

        }

        [HttpPost]
        public ActionResult SubmitNoCV(NoResumeSubmitViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    UserInputService service = new UserInputService();
                    int InsertedValue = service.InsertResumeDetailsIntoDB(model);
                    if (InsertedValue > 0)
                    {

                        // service.UpdateFilePathById(InsertedValue, path);
                        ZohoMailService z = new ZohoMailService();
                        bool successEmail = z.SendConfirmationUploadEmail(InsertedValue, model.Email, model.FullName);
                        ViewBag.scripCall = "DisplayMsgAfterUpload('Thank you! Your application has been submitted successfully, we will contact you when we found a job matching for you.');";
                    
                        //ViewBag.Message = "'upload success'";
                        //model.FilePath = path;
                    }
                     else
                    { //error insert into db 
                        ViewBag.scripCall = "DisplayMsgAfterUpload('Upload Error : Error DB01 - Please try again later or contact administrator if the problem persist.');";
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.scripCall = "DisplayMsgAfterUpload('upload failed : " + ex.Message + "');";
                    log.WriteException(ex, "SubmitNoCV");
                    //ViewBag.Message = ex.Message;
                    //throw;
                }
                //ModelState.Clear();

                ViewBag.scripCall = ViewBag.scripCall + "  window.location.href=\"/\"; ";
               
               // ViewBag.scriptCall = ViewBag.scriptCall + "     window.location.href='Home';";
                return View("Index");
                //return RedirectToAction("Index");
            }
            else
            {
                ViewBag.scripCall = "DisplayMsgAfterUpload('Submit failed : invalid ModelState, please try again.');";
                ViewBag.scripCall = ViewBag.scripCall + "  window.location.href=\"/\"; ";
                //ModelState.Clear();
                return View("Index");
                //return RedirectToAction("Index");
            }

        }


    }
}