﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoHire.Web.Models
{
    public class DefaultData
    {
        private static Dictionary<string, string> _PlaceOfResidence = null;
        private static Dictionary<string, string> _EducationLevel = null;
        private static Dictionary<int, string> _ExperienceYears = null;
        private static Dictionary<string, string> _CurrentPosition = null;
        private static Dictionary<string, string> _PreferredPosition = null;

        public static Dictionary<string, string> PlaceOfResidence {
            get {
                if (_PlaceOfResidence == null)
                    return getPlaceOfResidence();
                else
                    return _PlaceOfResidence;
            }
        }
        public static Dictionary<string, string> EducationLevel
        {
            get
            {
                if (_EducationLevel == null)
                    return getEducationLevel();
                else
                    return _EducationLevel;
            }
        }
        public static Dictionary<int, string> ExperienceYears
        {
            get
            {
                if (_ExperienceYears == null)
                    return getExperienceYears();
                else
                    return _ExperienceYears;
            }
        }
        public static Dictionary<string, string> CurrentPosition
        {
            get
            {
                if (_CurrentPosition == null)
                    return getCurrentPosition();
                else
                    return _CurrentPosition;
            }
        }
        public static Dictionary<string, string> PreferredPosition
        {
            get
            {
                if (_PreferredPosition == null)
                    return getPreferredPosition();
                else
                    return _PreferredPosition;
            }
        }


        private static Dictionary<string, string> getPlaceOfResidence()
        {
            Dictionary<string, string> item = new Dictionary<string, string>();

            item.Add("", "");
            item.Add("KL", "KL");
            item.Add("Selangor", "Selangor");
            item.Add("Perak", "Perak");
            item.Add("Johor", "Johor");
            item.Add("NS", "Negeri Sembilan");
            item.Add("Kedah", "Kedah");
            item.Add("Penang", "Penang");
            item.Add("Melaka", "Melaka");
            item.Add("Pahang", "Pahang");
            item.Add("Kelantan", "Kelantan");
            item.Add("Terrengganu", "Terrengganu");
            item.Add("Sabah", "Sabah");
            item.Add("Sarawak", "Sarawak");

            _PlaceOfResidence = item;
            return _PlaceOfResidence;
        }

        private static Dictionary<string,string> getEducationLevel()
        {
            Dictionary<string, string> item = new Dictionary<string, string>();

            item.Add("", "");
            item.Add("SPM", "SPM");
            item.Add("Diploma", "Diploma");
            item.Add("AdvanceDiploma", "Advance Diploma");
            item.Add("Degree", "Degree");
            item.Add("Master", "Master Degree");
            item.Add("Others", "Others (Free Text)");
            //SPM Diploma Advance Diploma Master Others(Free Text)

            _EducationLevel = item;
            return _EducationLevel;
        }


        private static Dictionary<int, string> getExperienceYears()
        {
            Dictionary<int, string> item = new Dictionary<int, string>();

            item.Add(0, "");
            item.Add(1, "No experience");
            item.Add(2, "1-3 years");
            item.Add(4, "4-6 years");
            item.Add(7, ">6years");

            _ExperienceYears = item;
            return _ExperienceYears;
        }

        private static Dictionary<string,string> getCurrentPosition()
        {
            Dictionary<string, string> item = new Dictionary<string, string>();

            item.Add("", "");
            item.Add("Technician", "Technician");
            item.Add("ServiceAdvisor", "Service Advisor");
            item.Add("ServiceManager", "Service Manager");
            item.Add("SalesAdvisor", "Sales Advisor");
            item.Add("SalesManager", "Sales Manager");
            item.Add("SparePartsAdvisor", "Spare Parts Advisor");
            item.Add("Trainer", "Trainer");
            item.Add("RegionalManager", "Regional Manager");
            item.Add("Others", "Others (Free Text)");
            //Technician	Service Advisor	Service Manager	Sales Advisor	Sales Manager	Spare Parts Advisor	Trainer	Regional Manager	Director	Others (Free Text)

            _CurrentPosition = item;
            return _CurrentPosition;
        }

        private static Dictionary<string,string> getPreferredPosition()
        {
            Dictionary<string, string> item = new Dictionary<string, string>();
            item.Add("", "");
            item.Add("Technician", "Technician");
            item.Add("ServiceAdvisor", "Service Advisor");
            item.Add("ServiceManager", "Service Manager");
            item.Add("SalesAdvisor", "Sales Advisor");
            item.Add("SalesManager", "Sales Manager");
            item.Add("SparePartsAdvisor", "Spare Parts Advisor");
            item.Add("Trainer", "Trainer");
            item.Add("RegionalManager", "Regional Manager");
            item.Add("Others", "Others (Free Text)");
            //Technician	Service Advisor	Service Manager	Sales Advisor	Sales Manager	Spare Parts Advisor	Trainer	Regional Manager	Director	Others (Free Text)

            _PreferredPosition = item;
            return _PreferredPosition;
        }
    }
}