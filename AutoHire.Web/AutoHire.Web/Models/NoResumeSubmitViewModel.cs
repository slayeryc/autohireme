﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Foolproof;

namespace AutoHire.Web.Models
{
    public class NoResumeSubmitViewModel
    {
        [Required(ErrorMessage = "Name is required")]
        public string FullName { get; set; }

        [Required(ErrorMessage ="Please input email address")]
        [EmailAddress(ErrorMessage = "Invalid Email Address format")]
        public string Email { get; set; }

        [RegularExpression(@"^[0-9-]*$", ErrorMessage = "please enter a valid Phone number")]
        [StringLength(18, MinimumLength = 7, ErrorMessage = "please enter length between 7-18")]
        public string PhoneNumber { get; set; }

        //[Required]
        //public string FilePath { get; set; }

        /*only without CV fields*/
        [DataType(DataType.Date)]
        [Required(ErrorMessage ="Please input Date of Birth")]
        public DateTime? DOB { get; set; }

        public string PlaceOfResidence { get; set; }
        public string EducationLevel { get; set; }
        [RequiredIf("EducationLevel", "Others", ErrorMessage = "Please input a position")]
        public string EducationLevelOther { get; set; }
        public int ExperienceYears { get; set; }
        public string CurrentPosition { get; set; }
        [RequiredIf("CurrentPosition", "Others",  ErrorMessage = "Please input a position")]
        public string CurrentPositionOther { get; set; }
        //public Dictionary<string, string> PreferredPositions { get; set; }
        public string PreferredPosition { get; set; }
        [RequiredIf("PreferredPosition", "Others", ErrorMessage = "Please input a position")]
        public string PreferredPositionOther { get; set; }

        public int? ExpectedSalary { get; set; }

    }
}