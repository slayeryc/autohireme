﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AutoHire.Web.Models
{
    public class UploadMainModel
    {
        [Required(ErrorMessage = "Name is required")]
        public string FullName { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Required(ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }

        [RegularExpression(@"^[0-9-]*$", ErrorMessage = "please enter a valid Phone number")]
        [StringLength(18, MinimumLength = 7, ErrorMessage = "please enter length between 7-18")]
        public string PhoneNumber { get; set; }

        //[Required]
        public string FilePath { get; set; }

        ///*only without CV fields*/
        //public DateTime DOB { get; set; }
        //public string PlaceOfResidence { get; set; }
        //public string EducationLevel { get; set; }
        //public int ExperienceYears { get; set; }
        //public string CurrentPosition { get; set; }
        ////public Dictionary<string, string> PreferredPositions { get; set; }
        //public string PreferredPosition { get; set; }
        //public int ExpectedSalary { get; set; }

        public HttpPostedFileBase uploadFile { get; set; }

        
        public string PreferredPosition { get; set; }
        [RequiredIf("PreferredPosition", "Others", ErrorMessage = "Please input a position")]
        public string PreferredPositionOther { get; set; }

    }

  
}