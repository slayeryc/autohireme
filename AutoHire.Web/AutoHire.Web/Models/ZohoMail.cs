﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoHire.Web.Models
{
    public class ZohoMail
    {
        public string fromAddress { get; set; } //Mandatory  
        public string toAddress { get; set; } //Mandatory  
        public string ccAddress { get; set; }
        public string bccAddress { get; set; }
        public string subject { get; set; }
        public string encoding { get; set; } //Big5;EUC-JP;EUC-KR;GB2312; ISO-2022-JP;ISO-8859-1;KOI8-R;Shift_JIS; US-ASCII;UTF-8(default);WINDOWS-1251;X-WINDOWS-ISO2022JP
        public string mailFormat { get; set; } //html(default) or plaintext
        public string content { get; set; }
    }
}