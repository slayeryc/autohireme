﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace AutoHire.Web.Services
{
    public class LoggingService
    {
        private void WriteString(string value)
        {
            string DirectoryPath = Path.GetDirectoryName(GeneralSettingsService.logFilePath);
            if (Directory.Exists(DirectoryPath))
                Directory.CreateDirectory(DirectoryPath);

            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    using (StreamWriter sw = new StreamWriter(GeneralSettingsService.logFilePath, true, System.Text.Encoding.Unicode))
                    {
                        sw.WriteLine(value);
                        sw.WriteLine();
                        sw.WriteLine();
                    }
                }
                catch (Exception)
                {}
            }
        }

        public void LogInfoIntoTextFile(string FunctionName, string message)
        {
            StringBuilder builder = new StringBuilder(1024);
            builder.AppendLine(String.Format("{0} - Info ", FunctionName));
            builder.AppendLine("-----------------------------------------------");
            builder.AppendLine("Date : " + DateTime.Now.ToLongDateString());
            builder.AppendLine("Time : " + DateTime.Now.ToLongTimeString());

            HttpContext context = HttpContext.Current;

            if (context != null)
            {
                builder.AppendLine("Path : " + context.Request.Path);
                builder.AppendLine("Raw URL : " + context.Request.RawUrl);
                builder.AppendLine("Forms :" + context.Request.Form.ToString());
                builder.AppendLine("Http method : " + context.Request.HttpMethod);
            }

            builder.AppendLine("Error Message : " + message);
            builder.AppendLine();
            builder.AppendLine("Stack Trace : ");
            builder.AppendLine(Environment.StackTrace);
            builder.AppendLine();

            this.WriteString(builder.ToString().Trim());
        }

        private void AppendSqlExceptionDetails(StringBuilder buffer, Exception ex)
        {
            SqlException sql = ex as SqlException;

            if (sql != null)
            {
                buffer.AppendLine();
                buffer.AppendLine("SQL Exception Detail :");
                buffer.AppendLine(String.Format("Error Code : {0}", sql.ErrorCode));
                buffer.AppendLine(String.Format("Line number : {0}", sql.LineNumber));
                buffer.AppendLine(String.Format("Procedure : {0}", sql.Procedure));
                buffer.AppendLine(String.Format("Number : {0}", sql.Number));
                buffer.AppendLine(String.Format("Server : {0}", sql.Server));
                buffer.AppendLine(String.Format("Source : {0}", sql.Source));
                buffer.AppendLine();
            }
        }

        public void WriteException(Exception exception, string customMessage)
        {
            StringBuilder builder = new StringBuilder(1024);
            builder.AppendLine("Error");
            builder.AppendLine("-----------------------------------------------");
            builder.AppendLine("Date : " + DateTime.Now.ToLongDateString());
            builder.AppendLine("Time : " + DateTime.Now.ToLongTimeString());

            HttpContext context = HttpContext.Current;

            if (context != null)
            {
                builder.AppendLine("Path : " + context.Request.Path);
                builder.AppendLine("Raw URL : " + context.Request.RawUrl);
                builder.AppendLine("Forms :" + context.Request.Form.ToString());
                builder.AppendLine("Http method : " + context.Request.HttpMethod);
            }

            builder.AppendLine("Exception Type : " + exception.GetType().Name);
            builder.AppendLine("Source : " + exception.Source);
            builder.AppendLine("Message : " + exception.Message);
            builder.AppendLine();
            builder.AppendLine("Stack Trace : ");

            builder.AppendLine(exception.StackTrace);
            builder.AppendLine();

            if (!string.IsNullOrEmpty(customMessage))
            {
                builder.AppendLine("Custom message :");
                builder.AppendLine(customMessage);
            }

            if (exception.InnerException != null)
            {
                Exception inner = exception.InnerException;
                builder.AppendLine();
                builder.AppendLine("---------------------------------------------------------------------------------------------------");
                builder.AppendLine("InnerException: ");
                builder.AppendLine("Exception Type : " + inner.GetType().Name);
                builder.AppendLine("Source : " + inner.Source);
                builder.AppendLine("Message : " + inner.Message);
                builder.AppendLine();
                builder.AppendLine("Stack Trace : ");
                builder.AppendLine(inner.StackTrace);
            }

            AppendSqlExceptionDetails(builder, exception);
            this.WriteString(builder.ToString());
        }

    }
}