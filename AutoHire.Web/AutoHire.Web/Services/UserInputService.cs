﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using AutoHire.Web.Models;

namespace AutoHire.Web.Services
{
    public class UserInputService
    {
        private LoggingService log = new LoggingService();

        //private static readonly string connectionString = "server=127.0.0.1;uid=root;database=auto1";
        public int InsertCVDetailsIntoDB(UploadMainModel data)
        {
            //string connectionString = "server=127.0.0.1;uid=root;database=auto1";
            MySqlConnection connection = null;
            int insertedId = 0;
            try
            {
                connection = new MySqlConnection(GeneralSettingsService.connectionString);
                connection.Open();
                MySqlCommand sqlcmd = new MySqlCommand();
                sqlcmd.Connection = connection;
                sqlcmd.CommandText = "insert into useruploadfiles(FullName, Email, PhoneNumber, CreatedDate_UTC, PreferredPosition) " +
                                        " values(@FullName, @Email, @PhoneNumber, utc_timestamp(), @PreferredPosition); " +
                                        " select LAST_INSERT_ID() as InsertedId;";
                //sqlcmd.Parameters.AddWithValue("@FilePath", data.FilePath);
                sqlcmd.Parameters.AddWithValue("@Email", data.Email);
                sqlcmd.Parameters.AddWithValue("@FullName", data.FullName);
                sqlcmd.Parameters.AddWithValue("@PhoneNumber", data.PhoneNumber);

                string prePosition = "";
                if (data.PreferredPosition != null && data.PreferredPosition.Equals("Others"))
                    prePosition = "Others - " + data.PreferredPositionOther;
                else
                    prePosition = data.PreferredPosition;

                sqlcmd.Parameters.AddWithValue("@PreferredPosition", prePosition);


                MySqlDataReader reader = sqlcmd.ExecuteReader();
                if(reader.Read())
                {
                    insertedId = int.Parse(reader["InsertedId"].ToString());
                }

                return insertedId;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                log.WriteException(ex, "InsertCVDetailsIntoDB");
                return 0;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        public bool UpdateFilePathById(int id, string FilePath)
        {
            MySqlConnection connection = null;

            try
            {
                connection = new MySqlConnection(GeneralSettingsService.connectionString);
                connection.Open();
                MySqlCommand sqlcmd = new MySqlCommand();
                sqlcmd.Connection = connection;
                sqlcmd.CommandText = "UPDATE useruploadfiles SET FilePath  = @FilePath WHERE id = @Id; ";
                sqlcmd.Parameters.AddWithValue("@FilePath", FilePath);
                sqlcmd.Parameters.AddWithValue("@Id", id);

                sqlcmd.ExecuteNonQuery();

                return true;

            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                log.WriteException(ex, "UpdateFilePathById");
                throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        public int InsertResumeDetailsIntoDB(NoResumeSubmitViewModel data)
        {
            //string connectionString = "server=127.0.0.1;uid=root;database=auto1";
            MySqlConnection connection = null;
            int insertedId = 0;
            try
            {
                connection = new MySqlConnection(GeneralSettingsService.connectionString);
                connection.Open();
                MySqlCommand sqlcmd = new MySqlCommand();
                sqlcmd.Connection = connection;
                sqlcmd.CommandText = "insert into UserResumeDetails(FullName, Email, PhoneNumber, DOB, PlaceofResidence, " +
                    " EducationLevel, ExperienceYears, CurrentPosition, PreferredPosition, ExpectedSalary,CreatedDate_UTC) " +
                     " values(@FullName, @Email, @PhoneNumber, @DOB, @PlaceOfResidence, " +
                     " @EducationLevel, @ExperienceYears, @CurrentPosition, @PreferredPosition, @ExpectedSalary, utc_timestamp()); " +
                                        " select LAST_INSERT_ID() as InsertedId;";
                //sqlcmd.Parameters.AddWithValue("@FilePath", data.FilePath);
                string eduLevel = "";
                if (data.EducationLevel != null && data.EducationLevel.Equals("Others"))
                    eduLevel = "Others - " + data.EducationLevelOther;
                else
                    eduLevel = data.EducationLevel;

                string curPosition = "";
                if (data.CurrentPosition != null &&  data.CurrentPosition.Equals("Others"))
                    curPosition = "Others - " + data.CurrentPositionOther;
                else
                    curPosition = data.CurrentPosition;

                string prePosition = "";
                if (data.PreferredPosition != null &&  data.PreferredPosition.Equals("Others"))
                    prePosition = "Others - " + data.PreferredPositionOther;
                else
                    prePosition = data.PreferredPosition;

                sqlcmd.Parameters.AddWithValue("@Email", data.Email);
                sqlcmd.Parameters.AddWithValue("@FullName", data.FullName);
                sqlcmd.Parameters.AddWithValue("@PhoneNumber", data.PhoneNumber);
                sqlcmd.Parameters.AddWithValue("@DOB", data.DOB);
                sqlcmd.Parameters.AddWithValue("@PlaceOfResidence", data.PlaceOfResidence);
                sqlcmd.Parameters.AddWithValue("@EducationLevel", eduLevel);
                sqlcmd.Parameters.AddWithValue("@ExperienceYears", data.ExperienceYears);
                sqlcmd.Parameters.AddWithValue("@CurrentPosition", curPosition);
                sqlcmd.Parameters.AddWithValue("@PreferredPosition", prePosition);
                sqlcmd.Parameters.AddWithValue("@ExpectedSalary", data.ExpectedSalary);

                MySqlDataReader reader = sqlcmd.ExecuteReader();
                if (reader.Read())
                {
                    insertedId = int.Parse(reader["InsertedId"].ToString());
                }

                return insertedId;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                log.WriteException(ex, "InsertResumeDetailsIntoDB");
                return 0;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

    }
}