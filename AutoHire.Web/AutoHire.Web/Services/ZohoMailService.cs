﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoHire.Web.Models;
using MySql.Data.MySqlClient;

namespace AutoHire.Web.Services
{
    public class ZohoMailService
    {
        private LoggingService log = new LoggingService();
        private static string EmailContent = "<html>    <head>        <title>Submission confirmation</title>    </head>    <body>        <table><tr><td><b>Hi</b></td></tr><tr><td><br></td>            <tr>                <td>Thank you so much for submitting your application with AutoHireMe.com.<br>We will contact you via email or phone once there's a matching job. <br>If you have any questions, please don't hesitate to reply to this email. We'd love to hear from you.                 </td>            </tr><tr><td><br></td></tr><tr><td>Thank you<br>AutoHireMe.com <br>support@autohireme.com <br>Copyright (c) 2018 AutoHireMe.com All Rights Reserved.</td></tr>        </table>    </body></html>";


        public bool SendEmail(Models.ZohoMail msg)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://mail.zoho.com/api/accounts/6542503000000008002/messages");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Zoho-authtoken", "aa01d8ec31718667cd485ac8bfb71a3a");


                    Task<HttpResponseMessage> response = client.PostAsJsonAsync<Models.ZohoMail>("messages", msg);
                    response.Wait();

                    HttpResponseMessage result = response.Result;

                    int statusCode = (int)result.StatusCode;
                    /*Code	Description
                        200	- SUCCESS - The API request was successful and completed without any errors. 
                        201	- CREATED - The API to create new resource was completed successfully and a new resource has been created. 
                        400	- BAD REQUEST - The input passed in the Request API is invalid or incorrect. The requestor has to change the input parameters and send the Request again.
                        401	- UNAUTHORIZED - The request is a special request and the user who has requested the operation is not entitled to perform the requested operation.  
                        500	- INTERNAL ERROR - The request cannot be completed due an error that occured in the server, when processing the request. */
                    if (statusCode == 200)
                    {
                        //Console.WriteLine("success");
                    }
                    else
                    {
                        //Console.WriteLine("failed jor tim");
                        //Console.WriteLine("statusCode:" + statusCode.ToString());
                        log.LogInfoIntoTextFile("SendEmail", "return error code when sending email : msg.Email=" + msg.toAddress + ", statusCode=" + statusCode.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                log.WriteException(ex, "SendEmail");
                return false;
            }
            return true;
        }

        public bool SendConfirmationUploadEmail(int insertedId, string emailAddress, string fullName)
        {
            ZohoMail mailMsg = new ZohoMail();
            mailMsg.fromAddress = "support@autohireme.com";
            mailMsg.toAddress = emailAddress; //"w_yew_choong@yahoo.com";//
            mailMsg.subject = "Submission confirmation from AutoHireMe";
            mailMsg.content = EmailContent.Replace("<td><b>Hi</b></td>", "<td>Hi <b>" + fullName + "</b>,</td>");


            int EmailInsertedId = InsertEmailIntoDB(mailMsg, insertedId, EmailStatusCode.Sending);

            if (SendEmail(mailMsg)) //send email
                UpdateDBEmailStatus(EmailInsertedId, EmailStatusCode.Completed);
            else
                UpdateDBEmailStatus(EmailInsertedId, EmailStatusCode.Failed);

            return true;
        }

        private int InsertEmailIntoDB(ZohoMail msg, int InsertedId, EmailStatusCode status)
        {
            //string connectionString = "server=127.0.0.1;uid=root;database=auto1";
            MySqlConnection connection = null;
            int EmailInsertedId = 0;
            try
            {
                connection = new MySqlConnection(GeneralSettingsService.connectionString);
                connection.Open();
                MySqlCommand sqlcmd = new MySqlCommand();
                sqlcmd.Connection = connection;
                sqlcmd.CommandText = "insert into EmailMessage(UserUploadId, ToAddress, SubjectMsg, Content, StatusCode, LastUpdatedDate_UTC) " +
                                        " values(@InsertedId, @Email, @Subject, @Content, @StatusCode, utc_timestamp()); " +
                                         " select LAST_INSERT_ID() as InsertedId;"; ;
                //sqlcmd.Parameters.AddWithValue("@FilePath", data.FilePath);
                sqlcmd.Parameters.AddWithValue("@InsertedId", InsertedId);
                sqlcmd.Parameters.AddWithValue("@Email", msg.toAddress);
                sqlcmd.Parameters.AddWithValue("@Subject", msg.subject);
                sqlcmd.Parameters.AddWithValue("@Content", msg.content);
                sqlcmd.Parameters.AddWithValue("@StatusCode", (int)status);
                //sqlcmd.Parameters.AddWithValue("@StatusCode", );

                MySqlDataReader reader = sqlcmd.ExecuteReader();
                if (reader.Read())
                {
                    EmailInsertedId = int.Parse(reader["InsertedId"].ToString());
                }

                return EmailInsertedId;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.WriteException(ex, "InsertEmailIntoDB : InsertedId=" + InsertedId.ToString());
                return -1;
                //throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        private void UpdateDBEmailStatus(int emailInsertedId, EmailStatusCode status)
        {
            MySqlConnection connection = null;
            try
            {
                connection = new MySqlConnection(GeneralSettingsService.connectionString);
                connection.Open();
                MySqlCommand sqlcmd = new MySqlCommand();
                sqlcmd.Connection = connection;
                sqlcmd.CommandText = "UPDATE EmailMessage SET StatusCode = @StatusCode WHERE Id = @EmailId; ";

                sqlcmd.Parameters.AddWithValue("@StatusCode", (int)status);
                sqlcmd.Parameters.AddWithValue("@EmailId", emailInsertedId);
               
                sqlcmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                log.WriteException(ex, "UpdateDBEmailStatus : emailInsertedId=" + emailInsertedId.ToString());
                //throw;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }
    }

    enum EmailStatusCode
    {
        Pending = 1,
        Sending = 2,
        Completed = 3,
        Failed = 4
    }
}